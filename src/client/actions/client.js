import {
  RECEIVE_CLIENT,
  RECEIVE_CLIENTS,
  REMOVE_CLIENT,
  SET_CREATING_CLIENT,
  SET_CREATING_CLIENT_ERROR,
  CLEAR_CREATING_CLIENT_ERROR,
  SET_EDITING_CLIENT,
  SET_EDITING_CLIENT_ERROR,
  CLEAR_EDITING_CLIENT_ERROR
} from '../constants/ClientActionTypes';
import { callApi } from '../apiService';

/** ***************************************************************************
 * ACTION CREATORS
 **************************************************************************** */

export const receiveClient = client => ({
  type: RECEIVE_CLIENT,
  client
});

export const receiveClients = clients => ({
  type: RECEIVE_CLIENTS,
  clients
});

export const removeClient = id => ({
  type: REMOVE_CLIENT,
  id
});

export const setEditingClient = editing => ({
  type: SET_EDITING_CLIENT,
  editing
});

export const setEditingClientError = error => ({
  type: SET_EDITING_CLIENT_ERROR,
  error
});

export const clearEditingClientError = () => ({
  type: CLEAR_EDITING_CLIENT_ERROR
});

export const setCreatingClient = creating => ({
  type: SET_CREATING_CLIENT,
  creating
});

export const setCreatingClientError = error => ({
  type: SET_CREATING_CLIENT_ERROR,
  error
});

export const clearCreatingClientError = () => ({
  type: CLEAR_CREATING_CLIENT_ERROR
});

/** ***************************************************************************
 * THUNK ACTION CREATORS
 **************************************************************************** */

export const fetchClient = id => dispatch => callApi(`/client/${id}`, 'GET')
  .then((client) => {
    dispatch(receiveClient(client));
  });

export const fetchClients = () => dispatch => callApi('/client/', 'GET')
  .then((clients) => {
    dispatch(receiveClients(clients));
  });

export const createClient = client => (dispatch) => {
  dispatch(setCreatingClient(true));
  return callApi('/client/', 'POST', client)
    .then((json) => {
      dispatch(setCreatingClient(false));
      dispatch(receiveClient(json));
      return Promise.resolve(json.id);
    })
    .catch((e) => {
      dispatch(setCreatingClient(false));

      e.response.json().then((json) => {
        if (json.errors && json.errors.length) {
          return dispatch(setCreatingClientError(json.errors[0]));
        }

        return dispatch(setCreatingClientError({
          msg: 'Unable to create client.'
        }));
      });
      return Promise.reject();
    });
};

export const editClient = (id, client) => (dispatch) => {
  dispatch(setEditingClient(true));
  return callApi(`/client/${id}`, 'PUT', client)
    .then((json) => {
      dispatch(setEditingClient(false));
      dispatch(receiveClient(json));
      return Promise.resolve(json.id);
    })
    .catch((e) => {
      dispatch(setEditingClient(false));

      e.response.json().then((json) => {
        if (json.errors && json.errors.length) {
          return dispatch(setEditingClientError(json.errors[0]));
        }

        return dispatch(setEditingClientError({
          msg: 'Unable to update client.'
        }));
      });
      return Promise.reject();
    });
};

export const deleteClient = id => dispatch => callApi(`/client/${id}`, 'DELETE')
  .then(() => dispatch(removeClient(id)));
