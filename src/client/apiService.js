import axios from 'axios';
import {
  staticAuthToken
} from './globalConstant';

const callApi = async (apiUrl, apiMethod, apiData = null) => {
  const data = axios({
    method: apiMethod,
    url: apiUrl,
    data: apiData,
    headers: {
      authorizationToken: staticAuthToken,
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': window.location.origin
    }
  });
  return data;
};

export { callApi };
